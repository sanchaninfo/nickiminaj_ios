/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');

	// window.location.href = 'dashboard.html';
      var myDB = window.sqlitePlugin.openDatabase({name: 'AeomDB.db', location: 'default'}, null, null);
      myDB.transaction(function(transaction) {
          transaction.executeSql('CREATE TABLE IF NOT EXISTS aeomSettings (id integer primary key, key1 text, key2 text, key3 text,key4 text,key5 text)', [],
              function(tx, result) {
               console.log("Table created successfully:" + JSON.stringify(result));
              },
              function(error) {
                  // showAlert("Error occurred while creating the table.:" + error);
                  //$("#alertMsg").html('<div class="alert_blk"></div>');
                  alert(error);
              });
      });
     myDB.transaction(function(transaction) {
          transaction.executeSql('SELECT * FROM aeomSettings', [], function(tx, results) {
              var len = results.rows.length,
                  i;
              if (len > 0) {
                  for (i = 0; i < len; i++) {
                       $('.joinbtn').hide();

                      if(results.rows.item(i).key4)
                        {
                          $('#profile_pic').attr('src', results.rows.item(i).key4);

                        }

                     $('.uAvatar').show();
                     $('.preloader').hide();
                     showPage('HomePage');
                     

                  }
              }
          }, null);
      });

 
        var lock = new Auth0Lock(
            // All these properties are set in auth0-variables.js
            AUTH0_CLIENT_ID,
            AUTH0_DOMAIN 
         );
        var userProfile; 
        $('.joinbtn').click(function(e) {
          showPage('HomePage');
            e.preventDefault();
            lock.show({ socialBigButtons: true},function(err, profile, token) {
                if (err) {
                    // Error callback
                    alert("There was an error logging in");
                    showPage('HomePage');
                } else {
					   

                  var myDB = window.sqlitePlugin.openDatabase({name: 'AeomDB.db', location: 'default'}, null, null);
                  myDB.transaction(function(tx) {
                          var qry = "DELETE FROM aeomSettings";
                          tx.executeSql(qry, function() {
                              // console.log("Delete success");
                          });
                      }, function errorCB(err) {
                           showAlert("Error processing SQL: " + err.code);
                          lock.hide();
                          showPage('HomePage');
                      },
                      function() {
                          var deviceID = device.uuid;

                          myDB.transaction(function(transaction) {
                              var executeQuery = "INSERT INTO aeomSettings (key1, key2,key4) VALUES (?,?,?)";
                              var ProfilePIC=profile.picture;
                              transaction.executeSql(executeQuery, [token, profile.name, profile.picture], function(tx, result) {
                                       $.ajax({
                                           url: serverURL+"/mobileSignIn",
                                          type: "POST",
                                          data: JSON.stringify({
                                              "mobileSignIn": {
                                                  "token": token,
                                                  "id": deviceID
                                              }
                                          }),
                                          dataType: "json",
                                          contentType: "application/json; charset=utf-8",
                                          traditional: true,
                                          success: function(data) {
                                               UpdateID(data.accountResult._id,ProfilePIC);
                                          },
                                          error: function(jqXHR, textStatus, errorThrown) {
                                              lock.hide();
                                          },
                                          complete: function() {

                                          }
                                      });
                                  },
                                  function(error) {
                                       lock.hide();
                                   });
                          });

                      });




                    
                }
            });
            /*      lock.show({socialBigButtons: true});*/
        });


   /*       var token = "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiI2Yjg1NTdiOTMxMzk4ZTdjZWNlMjEzNTYwZmM0OTU1OTk2NTZhNTRjZmYxZDM4N2Q3NmIwMDM0NTIwMTgyNTNhfGNyZWF0ZWRfYXQ9MjAxNi0xMS0xMFQwMzo1NTo1OC4wMzg1NTUxNTUrMDAwMFx1MDAyNm1lcmNoYW50X2lkPXkydjZqem53cXNtZHd6enpcdTAwMjZwdWJsaWNfa2V5PTVuN3dmeHRubXFwOGZ3c3giLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMveTJ2Nmp6bndxc21kd3p6ei9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzL3kydjZqem53cXNtZHd6enovY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tL3kydjZqem53cXNtZHd6enoifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoic2FuY2hhbiIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6InNhbmNoYW4iLCJjdXJyZW5jeUlzb0NvZGUiOiJVU0QifSwiY29pbmJhc2VFbmFibGVkIjpmYWxzZSwibWVyY2hhbnRJZCI6InkydjZqem53cXNtZHd6enoiLCJ2ZW5tbyI6Im9mZiJ9";

BraintreePlugin.initialize(token,
    function () {
        console.log("init OK!");
        

          var options = {
  //  amount: "49.99",
  // primaryDescription: "Your Item"
};

BraintreePlugin.presentDropInPaymentUI(options, function (result) {

    if (result.userCancelled) {
        console.debug("User cancelled payment dialog.");
    }
    else {
        console.info("User completed payment dialog.");
        console.info("Payment Nonce: " + result.nonce);
        console.debug("Payment Result.", result);
    }
});
  
    },
    function (error) { console.error(error); });

*/


    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {

    }
};



app.initialize();

  function UpdateID(id,PP) {
           var myDB = window.sqlitePlugin.openDatabase({name: 'AeomDB.db', location: 'default'}, null, null);
      myDB.transaction(function(tx) {
          var qry = "UPDATE aeomSettings SET key3='" + id + "'";
          tx.executeSql(qry, function() {

          });
      }, function errorCB(err) {
           // showAlert("Error processing SQL: " + err.code);
          //$("#loading").hide();
      }, function() {
          //window.location.href = 'dashboard.html';
                      $('.joinbtn').hide();
                      if(PP) 
                        {
                         $('#profile_pic').attr('src', PP);
                       }
                      $('.uAvatar').show();
                      $('.preloader').hide();
                      showPage('HomePage');



      });

  }
